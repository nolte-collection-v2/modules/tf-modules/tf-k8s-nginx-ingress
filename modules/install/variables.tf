variable "namespace" {
  default = ""
}

variable "chart_version" {
  default = "3.3.0"
}


variable "extra_values" {
  default = {}
}
