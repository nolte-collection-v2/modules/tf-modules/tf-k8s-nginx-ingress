
locals {
  EXTRA_VALUES = {
  }
}


resource "helm_release" "release" {
  name       = "ingress-nginx"
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"
  version    = var.chart_version
  namespace  = var.namespace

  values = [
    yamlencode(local.EXTRA_VALUES),
    yamlencode(var.extra_values)
  ]

  set {
    name  = "controller.publishService.enabled"
    value = "true"
  }
}
